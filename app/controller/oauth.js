'use strict';
const BaseController = require('./base.js');

class AuthController extends BaseController {
  async authenticate() {
    const { ctx } = this;
    ctx.success(ctx.state.oauth.token);
  }
  async user() {
    const { ctx } = this;
    const users = require('../data/db_user.json');
    let user;
    users.forEach(e => {
      if (e.id === ctx.state.oauth.token.user.userId) {
        user = e;
        return;
      }
    });
    ctx.success(user);
  }
}
module.exports = AuthController;
