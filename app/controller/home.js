'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }
  async callback() {
    const { ctx } = this;
    ctx.success(ctx.request, 'success');
  }
}

module.exports = HomeController;
