'use strict';
const { Controller } = require('egg');

class BaseController extends Controller {
  async index() {
    const { ctx } = this;
    const { request } = ctx;

    ctx.showErr(2, request.url);
  }
  async home() {
    const { ctx } = this;

    ctx.redirect('/user/user');
  }
}

module.exports = BaseController;
