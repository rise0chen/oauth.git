'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, oAuth2Server: oauth } = app;

  router.get('/', 'home.index');
  router.get('/callback', 'home.callback');

  router.all('/api/oauth/authorize', oauth.authorize());
  router.all('/api/oauth/token', oauth.token());

  router.all('/api/oauth/authenticate', oauth.authenticate(), 'oauth.authenticate');
  router.all('/api/oauth/user', oauth.authenticate(), 'oauth.user');
};
