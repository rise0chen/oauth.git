'use strict';

module.exports = {
  success(data = {}, errmsg) {
    const { response } = this;

    response.body = {
      errno: 0,
      errmsg: errmsg || 'success',
      data,
    };
  },
  fail(errno = 1, errmsg, data = {}) {
    const { response } = this;

    response.body = {
      errno,
      errmsg: errmsg || 'fail',
      data,
    };
  },
  showErr(errno = 1, data = {}) {
    const { response } = this;

    response.body = {
      errno,
      errmsg: 'error',
      data,
    };
  },
};
