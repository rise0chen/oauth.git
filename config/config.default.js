/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1564281130620_1447';

  // add your middleware config here
  config.middleware = [];

  config.oAuth2Server = {
    grants: [ 'password', 'authorization_code' ],
    accessTokenLifetime: 72 * 60 * 60,
  };

  config.security = {
    csrf: {
      ignore: () => {
        return true;
      },
    },
  };

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
